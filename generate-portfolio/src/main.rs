use std::io::{stdin, Read};

use bart_derive::BartDisplay;
use serde::Deserialize;
use toml;

#[derive(Deserialize)]
struct Project {
    id:          String,
    title:       String,
    date:        String,
    short_descr: String,
    long_descr:  String,
    picture:     String,
    thumbnail:   String,
    link:        String,
    piccol:      String,
    thumbcol:    String,
}

#[derive(Deserialize, BartDisplay)]
#[template = "page.html"]
struct Page {
    projects:  Vec<Project>,
    lang:      String,
    footnotes: String,
    preface:   String,
}

fn main() {
    let mut spec_content = Vec::with_capacity(3400);
    stdin().read_to_end(&mut spec_content).unwrap();
    let page: Page = toml::from_slice(&spec_content).unwrap();
    print!("{}", page);
}
