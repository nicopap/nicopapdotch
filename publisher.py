#!/usr/bin/env python3
# This script uses the pyinotify library to watch the file system
# and automatically runs `make` to build the site when a file changes

from multiprocessing import Process
import time
import subprocess as sp
import pyinotify

class OnWriteHandler(pyinotify.ProcessEvent):
    def my_init(self, cwd, sources):
        self.cwd = cwd
        self.lastCall = time.monotonic()
        self.sources = sources


    def _run_cmd(self, filename : str):
        _, _, extension = filename.rpartition('.')
        if extension not in ["md", "svg", "html", "toml", "rs", "css","jpg","png"] :
            return
        thisTime = time.monotonic()
        print(time.strftime('%Hh %M:%S'))
        if thisTime - self.lastCall < 2.0:
            return
        self.lastCall = thisTime
        print(f'==> Modification detected in file {self.cwd}/{self.sources}/{filename}')
        def runMake():
            sp.run(executable='make', args=['make'], cwd=self.cwd)
        p = Process(target=runMake)
        p.start()


    def process_default(self, event : pyinotify.Event):
        filename = event.name
        self._run_cmd(filename)


def main(pathsToWatch):
    wm = pyinotify.WatchManager()
    for sourcePath in pathsToWatch:
        handler = OnWriteHandler(cwd='.', sources=sourcePath)
        notifier = pyinotify.Notifier(wm, default_proc_fun=handler)
        wm.add_watch(sourcePath, pyinotify.IN_MODIFY, auto_add=True, rec=True)
        print(f'==> Start monitoring {sourcePath} (type c^c to exit)')
    notifier.loop()


if __name__ == '__main__':
    main(["assets","site","generate-portfolio"])
