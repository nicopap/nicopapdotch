#!/usr/bin/python3
import sys
import subprocess as sp

def main():
    fullHeader = (
	'<div id="header">'
		+ '<div class="head" id="site-name"><a href="/">NicoPap.ch</a></div>'
		+ '<div class="head"><a href="/blogs"     >blogs</a></div>'
		+ '<div class="head"><a href="/portfolio" >portfolio</a></div>'
		+ '<div class="head"><a href="/about"     >about</a></div>'
		+ '<div class="head" class="mini"><a href="/resume.pdf">resumé</a></div>'
	+ '</div>'
    )
    sp.run(args=['sed', f's;##SITE_HEADER##;{fullHeader};'])

if __name__ == "__main__":
    main()

