# How to rewrite your undergrade C GBA embedded project to Rust

## Fixing `gba`

I started hacking on rust-console/gba the 13th of June. The `gba` crate relies
on the rust `asm!` marco in nightly. It was only recently changed (the 8th) and
the crate was broken. I actually contributed to the crate by migrating to the
new syntax. The new syntax is excellent. The old one used to crash the
compilers without explanations, had an arcane error-prone syntax and broke the
code by default (unless you added a `volatile` option). The new syntax is much
more user-friendly, provides great error messages and has a clear and
fool-proof syntax.

In fact, a lot of documentation in the `gba` crate explained how dangerous
using `asm!` was. I had to remove a lot of documentation, since none of those
warnings applied to the new syntax.
