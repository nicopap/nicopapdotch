# Druid

Druid is a rust multiplatform GUI toolkit. It is very simple and especially
doesn't try to innoviate: it's the dumbest and easiest to pickup UI framework
I've ever tried.

In its current state it's quite limited. It's missing crutial features. You can
combine widgets on the same layout à la $W_1 + W_2$ style. But not have several
widgets that can occupy the same space at different place in time.

But the cross-plateform story in rust is **amazing**. I had a prototype running
in 30 minutes that communicates with a backend server through websockets that
compiles both in WASM and native UI, with minimum plateform-specific code.

The other win in rust is how easy it is to fork crates (3rd party libraries),
try out patches and integrate them. When I was playing with Druid, the WASM
target PR was not yet merged. I just had to add a `git = https://github.com/...`
to the Cargo.toml to try it.

I've also did some experiments in Rust using the
[druid](https://github.com/xi-editor/druid) UI frame work and it's fresh
webassembly backend. It's amazing! **You can write the same code and it runs on
your desktop and the web, without the headeach of packaging electron**. Again,
you don't realize how good it is until you try it. Imagine the ease of
packaging of Go, with the expresiveness of Rust and the distribution options
of Javascript.

## Community

I was shocked by the ammount of effort that went into currating the PR for WASM
and adding them to the crate. And how nice the interactions were.
