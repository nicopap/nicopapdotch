<!-- TODO:
  Ideas: use an actual API for real-world code!
  Use something related to COVID or Humanitarian help for domain exemple
  Be the most accessible possible.
  Introduce how this plays with https://47degrees.github.io/fetch/docs
-->
# Title

I've been toying with functional programming for a while. Now that I have a job
where I have to do real things with functional programming, I am discovering
the weaknesses and strong points of the paradigm.

One Member of the category zoo that I never understood was the "Free" family of
type classes (Free Monad, Free Applicative).

One pain point of functional programming is effect management. Even in Scala
where you can trivially side-step the IO monad, you still have to deal with
`Future`s, `Either`s (result type), `Option`s or `Valid`s (from `cats`).

One of the applications I maintain is pretty much a "pick up data from X and Y
and put it together". This -- while the base usecase for programming -- is not
pleasant to work with in a strongly typed functional language. In this context,
I've become quite familiar with monad transformers, eg: `EitherT`. Monad
transformers don't make combining a diverse set of monads pleasant or easy to
read, they merly make it possible.

Using a monad transformer you will still need to define type aliases:

```scala
type ExtractM[A] = EitherT[Future, ExtractError, A]
```

And add on top of it all the adaptators needed to go from one monad to the
other. This is a lot of code and painfull programming.

```scala
for {
  bleh <- OptionT(fetchService1())
  result <- if (bleh != 0) {
    OptionT.liftT(Future.succesful("static"))
  } else {
    for {
      intermediate <- OptionT.liftT(fetchService2(bleh))
      result <- fetchService3(intermediate)
    } yield result
  }
  other = pureComputation(result)
} yield complete
```


## Simplicity VS API complexity

We have an API that exposes historical data for financial instruments. Each
instrument has thousands of data points (each with values queriable in the
past).

Our data model is usually a list of objects. So you would need to define a
function to query for a whole list for all the fields the object contains and
then build them into a list.

```scala
case class Instrument(price: Double, name: String, currency: Currency, isin: Isin)

def fetchInstruments(instruments: List[String]): ExtractM[List[Instrument]] =
  for {
  // TODO: finish example
  } yield ???
```

But what if now you need to create a `Map[Account, Seq[Instrument]]`?

<!-- TODO: Example here -->

<!-- TODO: Introduce and motivate transformers here -->

<!-- TODO: motivate the "one instrument fetched many time" exemple, show how
 nice and composable it is and then break it down -->

Clearly, the strategy is not composable, or at least only with a lot of
verbosity. Even with transformers, it's still quite verbose. And it becomes a
lot harder to deal with the ever-expanding type signatures.


### Complexity inherant to effects

The complexity is inherant to the problem domain: What we
just did here is express in the type system that complexity.

You don't see this complexity in less stricly typed languages, not because it's
not there, but because the repercution of ignoring it -- instead of preventing
the compilation of the program -- will lead to runtime errors. Missing
`Option`s lead to omnipresent `NullPointerException`s, missing `Future`s to
gimped concurrency etc.

A language can help surface the complexity. For exemple, Python and Javascript
introduced async/await syntax. Well, async/await makes possible concurrency,
but they introduced the same complexity we have been fighting against right
now.

Same goes with Java `Exception`s, we introduced a completly new layer of
concepts, made the same trade-offs that the `Either` monad. You can work around
the complexity by extending `Runtime` errors and not having to explicitly
annotate your functions as throwing, but that indeed means we will have runtime
errors...

### The trick

However, in all those programming languages, regardless of the way the
error/async/option monads are handled, there is a trick, a universal trick that
helps functional and OOP developers alike in their quest to **clean code**.

What we see here (talking about the `for { if }` complexity) is CODE, very
repetitive code. It's hard to realize, because it's part of the syntax now, and
this also makes it hard to abstract away into a function (any programmer's
tool to deal with complexity).

It is possible though. We need to approach the problem differently. We need to
ask the right question, the question you always ask yourself as a developer:
what's the actual problem at hand?

What we are trying to solve with those monad transformers is a sort of
`fetch -> validate -> combine` loop. The `fetch -> validate` is common to all
data points we fetch, the `combine` is unique to the data structure to build.
We could have a single implementation for the former and leave combinators to
the programmer for the latter. Like this:

```scala
sealed trait FetchCmd[A] {
  def execute(): Future[A] = exercise_left_to_the_reader(this)
}

private case class FetchList[A](list: List[FetchCmd[A]])
  extends FetchCmd[List[A]]

private case class FetchStatic[A](value: A)
  extends FetchCmd[A]

private case class FetchOp[A](f: String => A, instrument: String,  field: String)
  extends FetchCmd[A]

// And probably many more
// ...

def combine4[Class,A1,A2,A3,A4](
  ctor: (A1,A2,A3,A4) => Class,
  a1: FetchCmd[A1],
  a2: FetchCmd[A2],
  a3: FetchCmd[A3],
  a4: FetchCmd[A4],
): FetchCmd[Class] = FetchF4(ctor, a1, a2, a3, a4)

def traverse[A](list: List[FetchCmd[A]]): FetchCmd[List[A]] =
  FetchList[A](list)

def fetch[A: Parseable](instrument: String, field: String): FetchCmd[A] =
  FetchOp[A](implicitly[Parseable[A]].parse, instrument, field)

def static[A](value: A): FetchCmd[A] =
  FetchStatic[A](value)
```

Now, we have enough building blocks to crea

```scala
case class Instrument(price: Double, name: String, currency: Currency, isin: Isin)

def fetchInstruments(names: List[String]): FetchCmd[Instrument] = {
  traverse(names.map { name =>
    combine4(
      Instrument _,
      fetch[Double](name, "PX"),
      static(name),
      fetch[Currency](name, "CRNCY"),
      fetch[Isin](name, "ISN")
    )
  })
}
```

What we want is a basic set of building blocks that let us build our class and
then let specialized code do the whole fetching, validating



## Free Applicative comes in

1. fetch X from 1
2. fetch X from 1 and based on X fetch Y on 2
3. fetch X from 1, Z from 3 and based on X fetch Y on 2
4. fetch X,W from 1, Z_{1-->n} from 3 and combine all of those to know what to
   fetch from 2
5. etc..

