#!/usr/bin/python3
import sys
import subprocess as sp

def main(cssFile, portfolioCssFile):
    cssContent = open(cssFile).read()
    cssPfContent = open(portfolioCssFile).read()
    sp.run(args=['sed', f's|##CSS_HEADER##|{cssContent}|;s|##CSS_PORTFOLIO_EXTRA##|{cssPfContent}|'])

if __name__ == "__main__":
    main(cssFile=sys.argv[1], portfolioCssFile=sys.argv[2])

