# nicopap.ch

This is the source of my personal website, publicly visible at
[nicopap.ch](https://nicopap.ch). It isn't quite all of the site, as some pages
of the site are actually private.

This repository contains all the code and data necessary to generate the static
parts of my website.

## Requirements

To build the site, the following dependencies are required:
* [GNU make](https://www.gnu.org/software/make/) to build the whole site
  automatically (though one may extract the command from the `Makefile` and
  apply it manually)
* [Pandoc](https://pandoc.org) to render the markdown pages
* [Rust](https://www.rust-lang.org/) and cargo for the portfolio template
  generation.
* perl for truncating extraneous whitespaces in css and html files
* python3 (version 3.5 and up) to insert the header into a page.
  (the ##SITE_HEADER## thing)
* a basic POSIX-complient environement with utilities such as `find`, `awk` and
  `sed`

## Deployment

This is a simple static site. There isn't much point in making the deployment
script public. Anyway, if you are still curious, it looks like this:

```sh
#!/bin/bash
make clean
make all
scp -rC live/* user@server:nicopap.ch
```

## License

Graphical assets and markup in the `site` directory are under the [Creative
Commons BY-SA license](https://creativecommons.org/licenses/by-sa/4.0/), unless
specified otherwise. While code, such as asset generation facilities, is under
MIT. Please check the LICENSE file to see the terms of the MIT license.

* goat (`lambda.jpg`) derived from work by 4028mdk09 [license](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
* link icon (`link-icon.svg`) by shashank singh [license](https://creativecommons.org/licenses/by/3.0/us/legalcode)
* *The Binding Of Isaac: Afterbirth+* and related assets are property of ©
  Nicalis Inc and Edmund McMillen. (`lilium-demo.png`, `lilium.jpg`)
