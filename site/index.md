---
title: Nicola Papale
lang: en
description: "nicopap.ch is Nicola Papale's personal site. Discover Nicola Papale, and what he is up to."
---

Hello, I'm Nicola and this is my personal website (like the cool old days). If
you are interested in the person that I am, [look at the about page](about). If you are
interested in what I did, [look at my portfolio](portfolio).
