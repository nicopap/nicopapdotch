---
title: "About Nicola"
lang: en
description: "The best software engineer in point o-five acres around the Jet d'eau"
#header-include-files: build/aboutcss.html
---

Hello, I am Nicola Papale, I am a freelance developer working very hard with 
the hope to make the world a better place.

If you are a recruiter who need to read a list in a format you are used to,
don't waste your time, [go read my resume](resume.pdf). If you are a peer,
utterly bored, pretending to work or just are curious, feel free to read on.

I only have 5 years of programming behind me, but I have a rich and wide
variety of experience, going from bare-metal programming to enterprise
ERP, passing by compiler design and realtime online web games. I
have strong opinions about a few things, but I understand other people have
different experiences and it's extremely enriching to listen to them and learn
from them. It is important to remember to carefully listen and be interested in
whoever you are talking to has to say.

I strongly value computer autonomy. I feel the GAFAM took most of it away from
us and I want to pry everyone's independence back from them. Guess why
everyone hates Facebook? People feel powerless and hustled by a faceless
entity with motivations completely opposite their own. Centralized and closed
(as in not interoperable) software is antithesis to our democracies that value
self-determination and cooperation. We must build software that carry our
values and collude for democracy rather than against it.

My current activities are "limited" to two very ambitious projects both
involving graphic programming in rust. I can't help myself, I don't even have
enough time to look after myself, I'm addicted to getting things done.

You might like to see [what I did in the past](portfolio). You should look at
it, it's neatly presented (and degrades gracefully even with javascript
disabled) (btw you should really disable javascript, it's a very bad idea to
run arbitrary code from other people indiscriminately on your computer (btw I
I now am in possession of your credit card number and encrypting your hard drive
(just joking got you!)))

To get in touch, lookup `nicopap.ch` with `whois`, [for example with nic.ch](https://www.nic.ch/whois/),
my email address is in the records. Maybe I could read your equally boring
resume/portfolio website?

I'm active on the following websites:

:::{style.display="flex" style.flexWrap="wrap"}
[![](/social/github-octo.svg){height="18"}![github](/social/github-text.svg){height="18"}](https://github.com/nicopap/){.gitlink}
[![gitlab](/social/gitlab-full.svg){height=26}](https://gitlab.com/nicopap/){#gitlab-link .gitlink}
[![linkedIn](/social/linkedin.svg){height=20}](https://www.linkedin.com/in/nicola-papale-64959917a){#linked-link .gitlink}
:::
