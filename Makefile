.DEFAULT_GOAL = all
.PHONY: move_assets render_files all clean


PUBLIC_FILES = $(shell find site -type f -not -path '*/p/*')
CSS_TARGETS = $(patsubst assets/%,live/%,$(shell find assets -type f -name '*.css'))
ASSET_TARGETS = $(patsubst assets/%,live/%,$(shell find assets -type f -not -name '*.css'))
MARKDOWN_FILES = $(shell find site -type f -name '*.md')
RENDERED_FILES = $(MARKDOWN_FILES:site/%.md=live/%)
PORTFOLIO_SOURCES = generate-portfolio/Cargo.toml generate-portfolio/src/main.rs generate-portfolio/page.html
PORTFOLIO_TARGETS = live/portfolio
GENERATOR_EXEC = generate-portfolio/target/release/portfolio-generate
FIND_CSS_HEADERS = '/^.header-include-files:/s/^.header-include-files:\s*\(.*\)/\1/p'

$(GENERATOR_EXEC) : $(PORTFOLIO_SOURCES)
	cd generate-portfolio && cargo build --release

$(PORTFOLIO_TARGETS) : live/% : site/%.toml $(GENERATOR_EXEC) build/globalcss.html build/portfolioextracss.html
	mkdir -p $(@D)
	$(GENERATOR_EXEC) < $< \
		| ./write-head-lang.py \
		| ./write-css.py build/globalcss.html build/portfolioextracss.html\
		| perl -0777 -pe 's/([;{}:])\s+/\1/g' \
		| perl -0777 -pe 's/>\s+</></g' > $@

$(CSS_TARGETS) : live/% : assets/%
	mkdir -p $(@D)
	perl -0777 -pe 's|\s*([{},:;])\s*|\1|g' < $< > $@

$(ASSET_TARGETS) : live/% : assets/%
	mkdir -p $(@D)
	cp $< $@

build/%css.html : inline-css/%.css template-page.html
	mkdir -p build
	perl -0777 -pe 's|\s*([{},:;])\s*|\1|g' < $< | \
		awk 'BEGIN{printf "<style>"} {printf("%s",$$0)} END{printf "</style>"}' > $@

.SECONDEXPANSION:
$(RENDERED_FILES) : live/% : site/%.md build/globalcss.html $$(shell sed -n $$(FIND_CSS_HEADERS) site/%.md)
	mkdir -p build
	cat $(wordlist 2,99,$^) > build/extraheader.html
	mkdir -p $(@D)
	pandoc  --highlight-style kate \
		--standalone \
		--include-in-header build/extraheader.html \
		--template=template-page.html \
		--email-obfuscation=javascript \
		-f markdown+emoji+smart -t html5 $< \
		| ./write-head-lang.py \
		| perl -0777 -pe 's/([;{}])\s+/\1/g' \
		| perl -0777 -pe 's/>\s+</></g' > $@

live/sitemap.xml: $(PUBLIC_FILES)
	mkdir -p build
	echo -n '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' > build/sitemap_firstline
	echo -n '</urlset>' > build/sitemap_lastline
	find site -type f -not -path '*/p/*' -printf '<url><loc>|||%P</loc><lastmod>%TY-%Tm-%Td</lastmod></url>\n'\
		| sed 's,|||,https://nicopap.ch/,;s,\.[^/<.]\+<,<,;s,/index<,/<,' \
		| cat build/sitemap_firstline - build/sitemap_lastline \
		> $@


move_assets: $(ASSET_TARGETS) $(CSS_TARGETS)
render_files: $(RENDERED_FILES) $(PORTFOLIO_TARGETS)
all: move_assets render_files live/sitemap.xml
clean:
	rm -r live/*
	rm -r build
